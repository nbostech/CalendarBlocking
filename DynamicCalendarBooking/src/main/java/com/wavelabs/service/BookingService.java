package com.wavelabs.service;

import java.util.List;

import org.hibernate.Session;

import com.wavelabs.dao.BookingsDao;
import com.wavelabs.model.Bookings;
import com.wavelabs.model.Provider;
import com.wavelabs.model.TimeSlots;
import com.wavelabs.util.SessionUtil;

/**
 * This class contains the all the services of the bookings
 * 
 * @author tharunkumarb
 *
 */
public class BookingService {
	static Session session = SessionUtil.getSession();
	static BookingsDao bookingsdao = new BookingsDao();

	public static Bookings getBooking(int receiverid, int timeslotid) {
		return bookingsdao.getBooking(receiverid, timeslotid);
	}

	public static Bookings getBookingByStatus(int receiverid, int timeslotid) {
		return bookingsdao.getBookingByStatus(receiverid, timeslotid);
	}

	public static List<String> cancelForRemainingReceivers(List<TimeSlots> timeslotlist) {
		return bookingsdao.cancelBookingForRemaining(timeslotlist);
	}

	public static List<Bookings> getBookings(TimeSlots timeslot) {
		return bookingsdao.getBookingByTimeslot(timeslot);
	}

	public static Bookings getBookedBookings(TimeSlots timeSlots) {
		return bookingsdao.getBookedBookings(timeSlots);
	}

	public static List<Bookings> getBookings(Provider provider) {
		return bookingsdao.getBookings(provider);
	}

}
