package com.wavelabs.model;

/**
 * This class model for Ids
 * 
 * @author tharunkumarb
 *
 */
public class Ids {
	public Ids() {

	}

	private int receiver_id;
	private int timeslot_id;

	public int getReceiver_id() {
		return receiver_id;
	}

	public void setReceiver_id(int receiver_id) {
		this.receiver_id = receiver_id;
	}

	public int getTimeslot_id() {
		return timeslot_id;
	}

	public void setTimeslot_id(int timeslot_id) {
		this.timeslot_id = timeslot_id;
	}

}
