package com.wavelabs.model;

/**
 * This is enum for status
 * 
 * @author tharunkumarb
 *
 */
public enum Status {
	booked, available, process, empty, cancel, decline;
}
