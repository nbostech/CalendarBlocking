Service provider:
The service provider will have the functionalities like

1. Register
2. Generate calendar
3. Create availability
4. Confirm bookings
5. Modify bookings
6. Cancel bookings for particulars
7. Decline bookings

Service receiver:
The service receiver will have the functionalities like

1. Register
2. Book a slot
